<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function register()
	{
		$this->load->view('register');
		$usuario = $this->input->get('usuario');
    	$nombre = $this->input->get('nombre');
    	$apellido = $this->input->get('apellido');
		$regis = $this->User_model->registro($usuario, $nombre,$apellido);
    }


	public function listado()
	{
		$regis = $this->User_model->leer();
		$this->load->view('listado',$regis);
    }
}